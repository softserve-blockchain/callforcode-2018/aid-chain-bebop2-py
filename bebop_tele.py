from pyparrot.Bebop import Bebop
from pprint import pprint
import json
import os

already_added = False

def sensor_callback(args):
    drone, outfile = args
    global already_added
    try:
        sanitized = {k: v.decode('utf-8') if type(v) is bytes else v for k, v in drone.sensors.sensors_dict.items()}
        js = json.dumps(sanitized)
        if already_added:
            outfile.write(',\n')
        else:
            already_added = True
        outfile.write(js)
    except:
        print('failed to json encode')
        try:
            pprint(drone.sensors.sensors_dict)
        except:
            pass

bebop = Bebop()

print("connecting")
success = bebop.connect(3)
print(success)

if (success):
    try:
        os.rename('bebop-sensor-data.json', 'bebop-sensor-data.bak.json')
    except:
        pass
    with open('bebop-sensor-data.json', 'a') as outfile:
        outfile.write('[')
        bebop.set_user_sensor_callback(sensor_callback, (bebop, outfile))
        # print("turning on the video")
        # bebop.start_video_stream()

        print("sleeping", 60)
        bebop.smart_sleep(60)

        # print('taking off')
        # bebop.safe_takeoff(10)

        # # set safe indoor parameters
        # bebop.set_max_tilt(5)
        # bebop.set_max_vertical_speed(1)

        # # trying out the new hull protector parameters - set to 1 for a hull protection and 0 without protection
        # bebop.set_hull_protection(1)

        # print("Flying direct: Slow move for indoors")
        # bebop.fly_direct(roll=0, pitch=20, yaw=0, vertical_movement=0, duration=4)

        # print("sleeping", 10)
        # bebop.smart_sleep(10)

        # print('landing')
        # bebop.safe_land(10)

        # print(bebop.ask_for_state_update())

        print("DONE - disconnecting")
        # bebop.stop_video_stream()
        # print("sleeping", 5)
        # bebop.smart_sleep(5)
        bebop.disconnect()
        outfile.write(']')
else:
    print('failed to connect')